class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :physician_id
      t.integer :patient_id
      t.datetime :date
      t.text :reason
      t.text :notes
      t.integer :diagnostic_id

      t.timestamps
    end
  end
end
