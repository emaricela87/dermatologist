class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.float :code
      t.string :description
      t.float :fee

      t.timestamps
    end
  end
end
