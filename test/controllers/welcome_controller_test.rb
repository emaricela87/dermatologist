require 'test_helper'

class WelcomeControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get patient" do
    get :patient
    assert_response :success
  end

  test "should get physician" do
    get :physician
    assert_response :success
  end

  test "should get employee" do
    get :employee
    assert_response :success
  end

  test "should get appointment" do
    get :appointment
    assert_response :success
  end

  test "should get insurance" do
    get :insurance
    assert_response :success
  end

  test "should get invoice" do
    get :invoice
    assert_response :success
  end

  test "should get diagnostic" do
    get :diagnostic
    assert_response :success
  end

end
