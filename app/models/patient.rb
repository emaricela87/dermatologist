class Patient < ActiveRecord::Base

  has_many :physicians, :through => :appointments
  has_many :appointments

  belongs_to :insurance
end
