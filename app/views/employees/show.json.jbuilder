json.extract! @employee, :id, :first_name, :last_name, :street_address, :city, :state, :zip_code, :email, :phone, :created_at, :updated_at
