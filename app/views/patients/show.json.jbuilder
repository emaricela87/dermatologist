json.extract! @patient, :id, :first_name, :last_name, :street_address, :city, :state, :zip_code, :email, :phone, :insurance_id, :created_at, :updated_at
