json.extract! @appointment, :id, :physician_id, :patient_id, :date, :reason, :notes, :diagnostic_id, :created_at, :updated_at
