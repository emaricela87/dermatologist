json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :total, :date, :employee_id, :appointment_id
  json.url invoice_url(invoice, format: :json)
end
