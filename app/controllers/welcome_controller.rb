class WelcomeController < ApplicationController
  def index
  end

  def patient
  end

  def physician
  end

  def employee
  end

  def appointment
  end

  def insurance
  end

  def invoice
  end

  def diagnostic
  end
end
