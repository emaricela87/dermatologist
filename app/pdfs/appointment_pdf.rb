class AppointmentPdf < Prawn::Document

  def initialize(appointment)
    super(top_margin: 70)
    @appointment = appointment

    appointment_id

  end

  def appointment_id
    text "Statement \##{@appointment_id}"
    text  @appointment.patient.first_name
    text  @appointment.patient.last_name

  end
end